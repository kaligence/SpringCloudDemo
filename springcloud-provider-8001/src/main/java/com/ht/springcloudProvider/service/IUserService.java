package com.ht.springcloudProvider.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ht.springcloud.entity.User;
import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 */

public interface IUserService extends IService<User> {

    /*
    获取所有的用户
     */
    public List<User> getAllUsers();
}
