package com.ht.springcloudProvider.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ht.springcloud.entity.User;
import com.ht.springcloudProvider.mapper.UserMapper;
import com.ht.springcloudProvider.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 */

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public List<User> getAllUsers() {
        User user = new User();
        user.setStatus("s").setUserName("lian shi diao yong");
        return userMapper.selectList(null);
    }
}
