package com.ht.springcloudProvider.controller;

import com.ht.springcloud.entity.User;
import com.ht.springcloudProvider.service.IUserService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 */
@RestController
public class UserController {

    private static Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private IUserService userService;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping(value = "/getAllUsers")
    @HystrixCommand(fallbackMethod = "processHystrix_getAllUsers")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    public String processHystrix_getAllUsers() {
        logger.error("进入熔断方法");
        return "进入熔断方法";
    }
}

