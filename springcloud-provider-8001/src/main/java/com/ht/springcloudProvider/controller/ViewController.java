package com.ht.springcloudProvider.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * <p>
 * 页面 控制器
 * </p>
 */
@Controller
public class ViewController {

    private static Logger logger = LoggerFactory.getLogger(ViewController.class);
//    @ResponseBody
    @RequestMapping(value = "/page")
    public String pageView(HttpServletRequest request) {
//        return view("E:/IdeaProjects/springcloud/springcloud-client/src/main/resources/templates/page.html");
        if (request.getHeader("X-PJAX") == "true") {
            logger.info("pjax请求");
            return "page";
        }
        return "page";
    }

    @RequestMapping(value = "/pjaxtest")
    public String pjaxtestView() {
        return "pjaxtest";
    }

    public String view(String path){

        String line = null;
        BufferedReader br = null;
        StringBuffer sb = null;
        File htmlFile = new File(path);

        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(htmlFile),"UTF-8"));
            sb = new StringBuffer();

            while ((line=br.readLine())!=null){
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    @RequestMapping(value = "/react")
    public String react() {
        return "reactDemo";
    }

    @RequestMapping(value = "/vue")
    public String vue() {
        return "vueDemo";
    }
}

