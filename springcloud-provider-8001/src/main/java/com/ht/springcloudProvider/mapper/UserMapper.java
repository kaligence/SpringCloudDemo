package com.ht.springcloudProvider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ht.springcloud.entity.User;
import com.ht.springcloudProvider.util.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
@CacheNamespace(implementation= RedisCache.class,eviction=RedisCache.class)
public interface UserMapper extends BaseMapper<User> {

}
