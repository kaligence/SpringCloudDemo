package com.ht.springcloudProvider;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@EnableEurekaClient
@SpringBootApplication
@EnableCircuitBreaker
@Controller
@MapperScan("com.ht.springcloudProvider.mapper")
public class SpringcloudProvider8001Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudProvider8001Application.class, args);
    }

    @Value("${server.port}")
    String port;

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String test(@PathVariable("name") String name) {
        System.out.println("hello " + name + " ! , port " + port);
        return "home";
    }
}
