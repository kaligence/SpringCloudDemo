package com.youzheng.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

public class test {
	
//	private static final AtomicLong count = new AtomicLong(0L);
	/*private static LongAdder count = new LongAdder();
	private static int NUMBER = 10000;
	Long countPlus = 0L;

	public static void main(String[] args) {
		// Thread substractThread = new substractThread();
		SubstractThread substractThread = new SubstractThread();
		substractThread.start();

		for (int i = 0; i < NUMBER; i++) {
//			synchronized (substractThread) {
			count.increment();
//			}
		}

		while (substractThread.isAlive()) {
		}

		System.out.println(count);
	}

	public static class SubstractThread extends Thread {
		@Override
		public void run() {
			for (int j = 0; j < NUMBER; j++) {
//				synchronized (this) {
				count.decrement();
//				}
			}
		}
	}*/
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*信号量同步，Semaphore的用法*/
	public static void main(String[] args) {
		Semaphore semaphore = new Semaphore(3);
		CyclicBarrier cyclicBarrier = new CyclicBarrier(3);
		for (int i=1; i<=50; i++) {
			new SecurityCheckThread(i, semaphore).start();
//			new SecurityCheckThread(i, cyclicBarrier).start();
		}
	}

	public static class SecurityCheckThread extends Thread {
		private int seq;
		private Semaphore semaphore;
		private CyclicBarrier cyclicBarrier;
		
		public SecurityCheckThread(int seq, Semaphore semaphore) {
			this.seq = seq;
			this.semaphore = semaphore;
		}
		
		public SecurityCheckThread(int seq, CyclicBarrier cyclicBarrier) {
			this.seq = seq;
			this.cyclicBarrier = cyclicBarrier;
		}
		
		@Override
		public void run() {
			try {
				semaphore.acquire();
//				cyclicBarrier.await();
				System.out.println("No."+seq+"号乘客开始安检...");
//				Thread.sleep(1000);
				if (seq%2==0) {
//					Thread.sleep(1000);
					System.out.println("No."+seq+"号乘客,身上有违禁物品,不准通过！");
				}
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
//				cyclicBarrier.reset();
				System.out.println("No."+seq+"号乘客完成安检！");
				semaphore.release();
			}
		}
	}
	
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	/*countDownLatch的用法*/
	/*public static void main(String[] args) {
		final CountDownLatch countDownLatch = new CountDownLatch(5);
		
		new Thread() {
			@Override
			public void run() {
				System.out.println("线程1开始执行。。。");
				try {
					Thread.sleep(1000);
					System.out.println("线程1结束！");
					countDownLatch.countDown();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					
				}
			}
		}.start();
		
		new Thread() {
			@Override
			public void run() {
				System.out.println("线程2开始执行。。。");
				try {
					Thread.sleep(2000);
					System.out.println("线程2结束！");
					countDownLatch.countDown();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					
				}
			}
		}.start();
		
		
		
		try {
			System.out.println("主线程开始执行。。。"+System.currentTimeMillis());
			System.out.println("等待其他线程执行完>>>");
			countDownLatch.await();
			System.out.println("其他线程执行完毕！");
			Thread.sleep(2000);
			System.out.println("主线程结束！"+System.currentTimeMillis());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	/*------------------------------------------------------------------------------------------------------------------------------------------*/
	
//	public static void main(String[] args) {
//		String[] args1 = {"1","2","3","4","5"};
//		Integer[] args1 = {1,2,3,4,5};
//		System.out.println(args.length);
//		Arrays.asList(args1).stream().forEach(x -> System.out.println(x));
//		for (Integer x:args1) {
//			System.out.println(x);
//		}
//		List<String> list = Arrays.asList(args1);
//		list.set(0, "0");
//		list.stream().forEach(x -> System.out.println(x));
//		list.add("6");
//		list.remove(0);
//		list.clear();
//	}
	
//	public static void main(String[] args){        
//		ArrayList<String> masterList = new ArrayList<String>();
//		masterList.add("1");
//		masterList.add("2");
//		masterList.add("3");
//		masterList.add("4");
//		masterList.add("5");
//		
//		List<String> branchList = masterList.subList(0, 3);
//		
////		masterList.add("6");
////		masterList.remove(0);
////		masterList.clear();
//		
//		System.out.println(branchList.get(0));
//		
//		branchList.clear();
//		branchList.add("6");
//		branchList.add("7");
//		branchList.remove(0);
//		
//		System.out.println(masterList);
//	}
}
