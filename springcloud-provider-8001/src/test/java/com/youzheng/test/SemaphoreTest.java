package com.youzheng.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadPoolExecutor;

public class SemaphoreTest {
    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(3);
        for (int i = 1; i <= 5; i++) {
            SecurityCheckThread securityCheckThread = new SecurityCheckThread(i, semaphore);
            service.execute(securityCheckThread);
        }
    }

    private static class SecurityCheckThread implements Runnable {
        private int count;
        private Semaphore semaphore;

        public SecurityCheckThread(int count, Semaphore semaphore) {
            this.count = count;
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();

                System.out.println(count + "号乘客开始安检...");
                Thread.sleep(2000);
                if (count % 2 == 0) {
                    System.out.println(count + "号乘客携带违禁品，不准通过!");
                } else {
                    System.out.println(count + "号乘客放行");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                semaphore.release();
            }

        }
    }
}
