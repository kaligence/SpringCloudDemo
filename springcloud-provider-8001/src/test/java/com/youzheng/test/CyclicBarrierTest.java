package com.youzheng.test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CyclicBarrierTest {
    public static void main(String[] args) {
        ExecutorService service = Executors.newCachedThreadPool();
        final CyclicBarrier cyclicBarrier = new CyclicBarrier(2);
        for (int i = 0; i < 5; i++) {
            cbTest test = new cbTest(i, cyclicBarrier);
            service.execute(test);
        }
    }

    public static class cbTest implements Runnable {
        private int count;
        private CyclicBarrier cyclicBarrier;

        public cbTest(int count, CyclicBarrier cyclicBarrier) {
            this.count = count;
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            try {
                System.out.println(">>>"+Thread.currentThread().getName()+"正在写数据...");
                Thread.sleep(count*2000);
                System.out.println(">>>"+Thread.currentThread().getName()+"写数据完成！");
                cyclicBarrier.await();
                System.out.println(">>>所有线程写数据完成！");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }
}
