package com.youzheng.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DeadLockTest {

    public static void main(String[] args) {
        final Lock lockLeft = new ReentrantLock();
        final Lock lockRight = new ReentrantLock();

        new Thread() {
            @Override
            public void run() {
                if (lockLeft.tryLock()) {
                    lockLeft.lock();
                    System.out.println("leftTask拿到了left锁");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    lockRight.tryLock();
                    lockRight.lock();
                    System.out.println("leftTask拿到了right锁");
                    lockRight.unlock();
                    lockLeft.unlock();
                }
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                if (lockRight.tryLock()) {
                    lockRight.lock();
                    System.out.println("rightTask拿到了right锁");
                    lockLeft.tryLock();
                    lockLeft.lock();
                    System.out.println("rightTask拿到了left锁");
                    lockLeft.unlock();
                    lockRight.unlock();
                }
            }
        }.start();
    }
}
