package com.youzheng.test;

import java.util.concurrent.atomic.LongAdder;

public class Volatile {
    private static LongAdder count = new LongAdder();
    private static final int NUMBER = 1000000;

//    private static class SubstractThread2 extends Thread {
//        @Override
//        public void run() {
//            for (int i = 0; i < 10000; i++) {
//                count++;
//            }
//        }
//    }

    public static void main(String[] args) {
        SubstractThread substractThread = new SubstractThread();
//        SubstractThread2 substractThread2 = new SubstractThread2();

        substractThread.start();
//        substractThread2.start();

        for (int i = 0; i < NUMBER; i++) {
            count.increment();
        }
        while (substractThread.isAlive()) {
        }

        System.out.println(">>>" + String.valueOf(count));
    }

    private static class SubstractThread extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < NUMBER; i++) {
                count.decrement();
            }
        }
    }
}
