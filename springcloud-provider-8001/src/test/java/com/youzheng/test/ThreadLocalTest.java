package com.youzheng.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadLocalTest {
	public static ThreadLocal<String> threadLocal = new ThreadLocal<String>();

	public static void main(String[] args) {
		ExecutorService executor = Executors.newFixedThreadPool(1); 
		for (int i = 0; i < 2; i++) {
			ThreadLocalTestThread localTestThread = new ThreadLocalTestThread();
			executor.execute(localTestThread);
		}
	}

	public static class ThreadLocalTestThread extends Thread {
		private static boolean flag = true;
		@Override
		public void run() {
			try {
				if (flag) {
					threadLocal.set(this.getName() + "_session info.");
					flag = false;
				}
				System.out.println(this.getName() + " 线程是 " + threadLocal.get());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				threadLocal.remove();
			}
			
		}
	}

}
