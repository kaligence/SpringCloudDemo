package com.youzheng.test;

public class Singleton {


    /**
     * 懒汉式-懒汉式就是不在系统加载时就创建类的单例，而是在第一次使用实例的时候再创建
     * @return
     */
//    private static Singleton singleton = null;
//    private Singleton() {
//    }
//    public static synchronized Singleton getSingleton() {
//        if (singleton != null) {
//            singleton = new Singleton();
//        }
//        return singleton;
//    }
//-------------------------------------------------------------------------------------

    /**
     * 饿汉式-在加载类的时候就会创建类的单例，并保存在类中
     *
     * @return
     */
//    private static Singleton singleton = new Singleton();
//
//    private Singleton() {
//    }
//
//    public static Singleton getSingleton() {
//        return singleton;
//    }
//-------------------------------------------------------------------------------------
    /**
     *双重加锁机制-在懒汉式实现单例模式的代码中，有使用synchronized关键字来同步获取实例，保证单例的唯一性，但是上面的代码在每一次执行时都要进行同步和判断，
     * 无疑会拖慢速度，使用双重加锁机制正好可以解决这个问题
      */
    private static Singleton singleton = null;

    private Singleton() {
    }

    public static Singleton getSingleton() {
        if (singleton == null) {
            synchronized (Singleton.class){
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
}
