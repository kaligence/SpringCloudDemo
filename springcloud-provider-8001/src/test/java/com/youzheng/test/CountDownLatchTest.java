package com.youzheng.test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CountDownLatchTest {
    /*countDownLatch的用法*/
	public static void main(String[] args) {
        ExecutorService service = Executors.newWorkStealingPool();
//		final CountDownLatch countDownLatch = new CountDownLatch(2);
        final CountDownLatch cdOrder = new CountDownLatch(1);
        final CountDownLatch cdAnswer = new CountDownLatch(3);

        for (int i = 1; i <= 3; i++) {
            CdTest cdTest = new CdTest(Long.valueOf(i), cdOrder, cdAnswer);
            service.execute(cdTest);
        }

        System.out.println("上班了,开始瞎晃...");
        try {
            Thread.sleep(2000);
            System.out.println("老板来了");
            cdOrder.countDown();
            cdAnswer.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("下班了");
        service.shutdown();

//		new Thread() {
//			@Override
//			public void run() {
//				System.out.println("线程1开始执行。。。");
//				try {
//					Thread.sleep(1000);
//					System.out.println("线程1结束！");
//					countDownLatch.countDown();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				} finally {
//
//				}
//			}
//		}.start();
//
//		new Thread() {
//			@Override
//			public void run() {
//				System.out.println("线程2开始执行。。。");
//				try {
//					Thread.sleep(2000);
//					System.out.println("线程2结束！");
//					countDownLatch.countDown();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				} finally {
//
//				}
//			}
//		}.start();
//
//		try {
//			System.out.println("主线程开始执行。。。"+System.currentTimeMillis());
//			System.out.println("等待其他线程执行完>>>");
//			countDownLatch.await();
//			System.out.println("其他线程执行完毕！");
//			Thread.sleep(2000);
//			System.out.println("主线程结束！"+System.currentTimeMillis());
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	private static class CdTest implements Runnable{
	    private long i = 0;
        final CountDownLatch cdOrder;
        final CountDownLatch cdAnswer;

        public CdTest(long i, CountDownLatch cdOrder, CountDownLatch cdAnswer) {
            this.i = i;
            this.cdOrder = cdOrder;
            this.cdAnswer = cdAnswer;
        }

        @Override
        public void run() {
            System.out.println(">>>"+Thread.currentThread().getName()+"准备开始工作！");
            try {
                //TODO 可以用来模拟瞬时高并发
                cdOrder.await();
                System.out.println(">>>"+Thread.currentThread().getName()+"开始工作...");
                Thread.sleep(i*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                cdAnswer.countDown();
            }
            System.out.println(">>>"+Thread.currentThread().getName()+"结束工作！");
        }
    }
}
