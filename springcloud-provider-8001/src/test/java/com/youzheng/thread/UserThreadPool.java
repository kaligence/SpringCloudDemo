package com.youzheng.thread;

import com.google.protobuf.Internal;

import java.util.concurrent.*;
import java.util.concurrent.ThreadPoolExecutor.DiscardOldestPolicy;
import java.util.concurrent.locks.ReentrantLock;

public class UserThreadPool {
	public static void main(String[] args) {
		ReentrantLock reentrantLock = new ReentrantLock();
		BlockingQueue queue = new LinkedBlockingQueue(2);
//		BlockingQueue queue = new ArrayBlockingQueue<>(2);
		
		UserThreadFactory userThreadFactory1 = new UserThreadFactory("机房1");
		UserThreadFactory userThreadFactory2 = new UserThreadFactory("机房2");
		
		UserRejectedHandler handler = new UserRejectedHandler();
//		
		ThreadPoolExecutor threadPool1 = new ThreadPoolExecutor(1, 2, 60, TimeUnit.SECONDS, queue, userThreadFactory1, handler);
		ThreadPoolExecutor threadPool2 = new ThreadPoolExecutor(1, 2, 60, TimeUnit.SECONDS, queue, userThreadFactory2, handler);
//		ExecutorService threadPool3 = Executors.newWorkStealingPool();
//		
//		ThreadPoolExecutor threadPoolTest = new ThreadPoolExecutor(256, 256, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue(2500));
		
		Task task = new Task(); 
		Future<Integer> result;
//		ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(userThreadFactory1);
//		executorService.scheduleAtFixedRate(task, 5, 1, TimeUnit.SECONDS);
 		for (int i=0; i<10; i++) {
			threadPool1.execute(task);
//			threadPool2.execute(task);
//			result = threadPool1.submit(task);
//			try {
//				System.out.println(">>>"+String.valueOf(result.get()));
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}
// 			result = threadPoolTest.submit(task);
// 			threadPoolTest.submit(task);
// 			try {
//				Thread.sleep(1000);//此任务耗时1s
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
 		if (threadPool1 != null) {
			threadPool1.shutdown();
		}
	}
}
