package com.youzheng.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicLong;

//public class Task implements Callable<Integer> {
public class Task implements Runnable {
//public class Task extends Thread{

    //	private final AtomicLong count = new AtomicLong(0L);
    private int count = 1;


    public Task() {
    }

    public Task(int count) {
        this.count = count;
    }

    @Override
    public void run() {
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println(">>>" + System.currentTimeMillis());
    }
//    @Override
//        public Integer call() throws Exception {
////		synchronized(count){
////		Thread.sleep(10*1000);
////		System.out.println(">>>"+System.currentTimeMillis());
////		}
//        count++;
//        return Integer.valueOf(count);
//    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new Task(1), "t1");
        t1.start();
        try {
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(">>>join()结束之后");
    }

}
