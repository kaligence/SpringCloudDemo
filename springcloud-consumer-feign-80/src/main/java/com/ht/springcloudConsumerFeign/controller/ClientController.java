package com.ht.springcloudConsumerFeign.controller;

import com.ht.springcloudConsumerFeign.service.HystrixClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class ClientController {

//    @Autowired
//    private ClientService service;
//


    @Resource
    private HystrixClient hystrixClient;

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String test(@PathVariable("name") String name) {
        return hystrixClient.test(name);
    }

    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public String getUserById(@PathVariable("id") Long id) {
        return hystrixClient.getUserById(id);
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.GET)
    public String getUserList() {
        return hystrixClient.getUserList();
    }

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.GET)
    public Boolean updateUser(@PathVariable("id") Long id) {
        return hystrixClient.updateUser(id);
    }

    @RequestMapping(value = "/page")
    public String pageView() {
        return hystrixClient.pageView();
    }

    @RequestMapping(value = "/pjaxtest")
    public String pjaxtestView() {
        return hystrixClient.pjaxtestView();
    }

    @RequestMapping(value = "/react")
    public String react() {
        return hystrixClient.react();
    }

    @RequestMapping(value = "/vue")
    public String vue() {
        return hystrixClient.vue();
    }

    @RequestMapping(value = "/getAllUsers")
    public String getAllUsers() {
        return hystrixClient.getAllUsers();
    }


}
