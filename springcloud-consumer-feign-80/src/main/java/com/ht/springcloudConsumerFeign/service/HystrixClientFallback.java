package com.ht.springcloudConsumerFeign.service;

import org.springframework.stereotype.Component;

@Component
public class HystrixClientFallback implements HystrixClient {
    @Override
    public String test(String name) {
        return "sorry " + name + " !";
    }

    @Override
    public String getUserById(Long id) {
        return errorHandle("getUserById");
    }

    @Override
    public String getUserList() {
        return errorHandle("getUserList");
    }

    @Override
    public Boolean updateUser(Long id) {
        return false;
    }

    @Override
    public String pageView() {
        return errorHandle("pageView");
    }

    @Override
    public String pjaxtestView() {
        return errorHandle("pjaxtestView");
    }

    @Override
    public String react() {
        return errorHandle("react");
    }

    @Override
    public String vue() {
        return errorHandle("vue");
    }

    @Override
    public String getAllUsers() {
        return errorHandle("getAllUsers");
    }

    public String errorHandle(String method) {
        return "method >" + method + "< error";
    }
}
