package com.ht.springcloudConsumerFeign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "client", fallback = HystrixClientFallback.class)
public interface HystrixClient {
    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public String test(@PathVariable("name") String name);

    @RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET)
    public String getUserById(@PathVariable("id") Long id);

    @RequestMapping(value = "/getUserList", method = RequestMethod.GET)
    public String getUserList();

    @RequestMapping(value = "/updateUser/{id}", method = RequestMethod.GET)
    public Boolean updateUser(@PathVariable("id") Long id);

    @RequestMapping(value = "/page")
    public String pageView();

    @RequestMapping(value = "/pjaxtest")
    public String pjaxtestView();

    @RequestMapping(value = "/react")
    public String react();

    @RequestMapping(value = "/vue")
    public String vue();

    @RequestMapping(value = "/getAllUsers")
    public String getAllUsers();
}
