package com.ht.springcloudConsumerFeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients/*(basePackages = {"com.ht.springcloud"})*/
//@ComponentScan("com.ht.springcloud")
@EnableEurekaClient
@EnableHystrix
@EnableHystrixDashboard
@SpringBootApplication
public class SpringcloudConsumerFeign80Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudConsumerFeign80Application.class, args);
	}
}
