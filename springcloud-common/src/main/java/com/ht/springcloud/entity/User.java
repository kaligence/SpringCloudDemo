package com.ht.springcloud.entity;

//import com.baomidou.mybatisplus.annotation.TableId;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author hutao
 * @since 2018-04-24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)//支持链式调用
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

//    @TableId(value = "openid", type = IdType.AUTO)
//    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
//    @TableId
    private Long openid;
//    @TableField("user_name")
//    @Column(name = "user_name")
    private String userName;
    private String status;
}
